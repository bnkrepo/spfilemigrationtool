﻿using System;

namespace SPFileMigrationTool.Log
{
    public enum LogLevel
    {
        Debug,
        Information,
        Warning, 
        Error
    }

    public class LogManager
    {
        public delegate void WriteLogDelegate(string strLog);
        public event WriteLogDelegate OnWriteLog;

        static LogManager logManager = null;
        internal bool ShowInfo { get; set; }
        internal bool ShowWarning { get; set; }
        internal bool ShowDebug { get; set; }
        internal bool ShowErrors { get; set; }

        public static LogManager Instance
        {
            get
            {
                if (logManager == null)
                    logManager = new LogManager();

                return logManager;
            }
        }

        private LogManager()
        {

        }

        public void WriteLog(string strLog, LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Debug:
                    WriteDebug(strLog);
                    break;
                case LogLevel.Information:
                    WriteInformation(strLog);
                    break;
                case LogLevel.Error:
                    WriteError(strLog);
                    break;
                case LogLevel.Warning:
                    WriteWarning(strLog);
                    break;
            }
        }

        public void WriteWarning(string strLog)
        {
            if (ShowWarning == true)
                OnWriteLog?.Invoke(Environment.NewLine + "Warning: " + strLog);
        }

        public void WriteError(string strLog)
        {
            if (ShowErrors == true)
                OnWriteLog?.Invoke(Environment.NewLine + "Error: " + strLog);
        }

        public void WriteInformation(string strLog)
        {
            if (ShowInfo == true)
                OnWriteLog?.Invoke(Environment.NewLine + "Info: " + strLog);
        }

        public void WriteDebug(string strLog)
        {
            if (ShowDebug == true)
                OnWriteLog?.Invoke(Environment.NewLine + "Debug: " + strLog);
        }
    }
}
