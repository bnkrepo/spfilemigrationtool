﻿using Microsoft.SharePoint.Client;
using SPFileMigrationTool.Log;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPFileMigrationTool
{
    //Source: https://sharepoint.stackexchange.com/questions/171184/upload-a-folder-with-sub-folders-and-files-recursively-with-pure-csom
    public class SP2K16FileHelper
    {
        public static void UploadDocument(ClientContext clientContext, string sourceFilePath, string serverRelativeDestinationPath)
        {
            using (var fs = new FileStream(sourceFilePath, FileMode.Open))
            {
                var fi = new FileInfo(sourceFilePath);

                if (clientContext.HasPendingRequest == true)
                {
                    clientContext.ExecuteQuery();
                }

                Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, serverRelativeDestinationPath, fs, true);
            }
        }

        public static void UploadFolder(ClientContext clientContext, DirectoryInfo sourceFolderInfo, Folder spFolder)
        {
            FileInfo[] files = null;
            DirectoryInfo[] subDirs = null;

            try
            {
                files = sourceFolderInfo.GetFiles("*.*");
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e.Message);
            }

            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }

            if (files != null)
            {
                foreach (FileInfo fi in files)
                {
                    Console.WriteLine(fi.FullName);
                    clientContext.Load(spFolder);
                    clientContext.ExecuteQuery();
                    UploadDocument(clientContext, fi.FullName, spFolder.ServerRelativeUrl + "/" + fi.Name);
                }

                subDirs = sourceFolderInfo.GetDirectories();

                foreach (DirectoryInfo dirInfo in subDirs)
                {
                    Folder subFolder = spFolder.Folders.Add(dirInfo.Name);
                    clientContext.ExecuteQuery();
                    UploadFolder(clientContext, dirInfo, subFolder);
                }
            }
        }

        public static void UploadFoldersRecursively(ClientContext clientContext, string sourceFolder, string destinationLibraryTitle)
        {
            Web web = clientContext.Web;
            var query = clientContext.LoadQuery(web.Lists.Where(p => p.Title == destinationLibraryTitle));
            clientContext.ExecuteQuery();
            List documentsLibrary = query.FirstOrDefault();
            var folder = documentsLibrary.RootFolder;
            DirectoryInfo di = new DirectoryInfo(sourceFolder);

            clientContext.Load(documentsLibrary.RootFolder);
            clientContext.ExecuteQuery();

            folder = documentsLibrary.RootFolder.Folders.Add(di.Name);
            clientContext.ExecuteQuery();

            UploadFolder(clientContext, di, folder);
        }
    }
}
