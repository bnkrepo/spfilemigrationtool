﻿using SPFileMigrationTool.FileProcessor;
using SPFileMigrationTool.Log;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace SPFileMigrationTool.DB
{
    public class SPDBHandler
    {
        string dbConnectionString = "";
        Dictionary<string, string> fileNameMap = new Dictionary<string, string>();

        public List<string> SimulatedDownloadFileList { get; set; }  = new List<string>();

        public SPDBHandler(string connectionStr)
        {
            dbConnectionString = connectionStr;
        }

        public Dictionary<string, string> DownloadSPFiles(Dictionary<string, MigrationData> migrationData, 
                                    string downloadFolderPath, 
                                    bool bSimulateMigration,
                                    Action<int, string> SetProgress,
                                    Action<string> SetStatusText)
        {
            fileNameMap.Clear();
            SimulatedDownloadFileList.Clear();

            if (string.IsNullOrEmpty(dbConnectionString) == true)
            {
                LogManager.Instance.WriteError("Invalid DB Connection string");
                return fileNameMap;
            }

            SetStatusText("Connecting to SQL DB...");

            try
            {
                using (SqlConnection con = new SqlConnection(dbConnectionString))
                {
                    con.Open();

                    using (SqlCommand command = con.CreateCommand())
                    {
                        string cmdText;
                        int fileCount;
                        fileCount = 0;

                        SetStatusText("Building the sql query...");
                        cmdText = GenerateSQLQuery(migrationData, bSimulateMigration);
                        command.CommandText = cmdText;
                        LogManager.Instance.WriteDebug(string.Format("Executing sql: {0}", cmdText));

                        var dbDataContents = new List<string>();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // grab the file’s directory and name
                                string dirName = (string)reader["DirName"];
                                string leafName = (string)reader["LeafName"];

                                try
                                {
                                    // create directory for the file if it doesn’t yet exist
                                    if (Directory.Exists(downloadFolderPath) == false)
                                    {
                                        Directory.CreateDirectory(downloadFolderPath);
                                        LogManager.Instance.WriteInformation("Creating directory: " + downloadFolderPath);
                                    }

                                    // create a filestream to spit out the file
                                    var migrationDataKey = dirName + "\\" + leafName;
                                    var fileName = leafName;
                                    var filePath = downloadFolderPath + "/" + fileName;

                                    if (dbDataContents.Contains(migrationDataKey) == true)
                                    {
                                        LogManager.Instance.WriteWarning(string.Format("Rejected duplicate entry: {0}", migrationDataKey));
                                        continue; // duplicate data found in the db; reject it.
                                    }
                                    else
                                        dbDataContents.Add(migrationDataKey);

                                    if ((bSimulateMigration == true && SimulatedDownloadFileList.Contains(fileName) == true) ||
                                        (bSimulateMigration == false && File.Exists(filePath) == true))
                                    {
                                        var uniqueName = GetUniqueFileName(fileName);
                                        fileName = uniqueName;
                                        filePath = downloadFolderPath + "/" + fileName;
                                        fileNameMap.Add(uniqueName, migrationDataKey);
                                    }
                                    else
                                        fileNameMap.Add(fileName, migrationDataKey);

                                    if (bSimulateMigration == true)
                                    {
                                        SimulateWriteFileToDisk(migrationData, migrationDataKey, fileName);
                                    }
                                    else
                                    {
                                        WriteFileToDisk(reader, fileName, filePath);
                                    }
                                }
                                catch (Exception exp)
                                {
                                    LogManager.Instance.WriteError("Exception while downloading: " + leafName);
                                    LogManager.Instance.WriteError(exp.Message);
                                }

                                fileCount++;
                                SetProgress(fileCount, string.Format("Processing {0} of {1} requests.", fileCount, migrationData.Count));
                            }

                            // close the DB connection and whatnots
                            reader.Close();
                        }

                        LogManager.Instance.WriteInformation(string.Format("Total files found in the databse: {0}", fileCount));

                        if (fileCount != migrationData.Count)
                        {
                            LogManager.Instance.WriteInformation(string.Format("Input data had {0} items.", migrationData.Count));
                            SetProgress(migrationData.Count, "Some files could not be retrieved. Check log for details.");
                        }
                        LogManager.Instance.WriteInformation("=========================================================================");
                    }
                }
            }
            catch(Exception exp)
            {
                LogManager.Instance.WriteError(exp.Message);
            }

            return fileNameMap;
        }

        private string GenerateSQLQuery(Dictionary<string, MigrationData> migrationData, bool bSimulateMigration)
        {
            var cmdText = "";

            if (bSimulateMigration == true)
            {
                cmdText = "select DirName, LeafName " +
                                        "from AllDocs " +
                                        "where ((";
            }
            else
            {
                cmdText = "select AllDocs.DirName, AllDocs.LeafName, DocStreams.Content " +
                                        "from AllDocs " +
                                        "inner join DocStreams on AllDocs.Id = DocStreams.Id " +
                                        "and AllDocs.SiteId = DocStreams.SiteId " +
                                        "where ((";
            }

            // Build the query            
            string cmdFileTypeStr = "AllDocs.DirName='{0}' and AllDocs.LeafName='{1}'";
            StringBuilder cmdExtStr = new StringBuilder();
            
            var migrationDataList = migrationData.Values.ToList();

            for (int i = 0; i < migrationDataList.Count; i++)
            {
                LogManager.Instance.WriteDebug(string.Format("Processing: {0}", migrationDataList[i].SourceFileNameWithPath));

                cmdExtStr.Append(string.Format(cmdFileTypeStr, migrationDataList[i].SourcePath.Replace("'", "''"), migrationDataList[i].SourceFileName.Replace("'", "''")));

                if (i < migrationDataList.Count - 1)
                    cmdExtStr.Append(") or (");
            }

            cmdExtStr.Append("))");
            cmdText += cmdExtStr.ToString();

            return cmdText;
        }

        private void SimulateWriteFileToDisk(Dictionary<string, MigrationData> migrationData, string migrationDataKey, string fileName)
        {
            SimulatedDownloadFileList.Add(fileName);

            if (migrationData.ContainsKey(migrationDataKey) == true)
            {
                LogManager.Instance.WriteInformation(string.Format("Saved (simulated): [{0}] >> {1}", migrationData[migrationDataKey].SourceFileName, fileName));
            }
            else
                LogManager.Instance.WriteInformation(string.Format("Saved (simulated): {0}", fileName));
        }

        private static void WriteFileToDisk(SqlDataReader reader, string fileName, string filePath)
        {
            FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);

            BinaryWriter writer = new BinaryWriter(fs);

            // depending on the speed of your network, you may want to change the buffer size (it’s in bytes)
            int bufferSize = 1000000;
            long startIndex = 0;
            long retval = 0;
            byte[] outByte = new byte[bufferSize];

            // grab the file out of the db one chunk (of size bufferSize) at a time
            do
            {
                retval = reader.GetBytes(2, startIndex, outByte, 0, bufferSize);
                startIndex += bufferSize;

                writer.Write(outByte, 0, (int)retval);
                writer.Flush();

            } while (retval == bufferSize);

            // finish writing the file
            writer.Close();
            fs.Close();

            LogManager.Instance.WriteInformation(string.Format("Saved: {0}", fileName));
        }

        private string GetUniqueFileName(string fileName)
        {
            var fileNameWOExt = Path.GetFileNameWithoutExtension(fileName);
            var ext = Path.GetExtension(fileName);

            return string.Format(@"{0} [{1}]{2}", fileNameWOExt, Guid.NewGuid(), ext);
        }
    }
}
