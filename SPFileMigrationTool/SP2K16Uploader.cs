﻿using Microsoft.SharePoint.Client;
using SPFileMigrationTool.Log;
using System;
using System.IO;
using System.Linq;

namespace SPFileMigrationTool
{
    public class SP2K16Uploader
    {
        string spSiteLink = "";
        string spDocLibName = "";

        public SP2K16Uploader()
        {

        }

        public bool Upload( string sourceFolder, string sourceFile, string destSiteLink, string destDocLibName, 
                            string destDocLibPath, string destinationFile, bool bSimulateMigration)
        {
            LogManager.Instance.WriteDebug(string.Format("{0} ==>", sourceFile));

            spSiteLink = destSiteLink;
            spDocLibName = destDocLibName;

            if (string.IsNullOrEmpty(spSiteLink) == true || string.IsNullOrEmpty(spDocLibName) == true)
            {
                LogManager.Instance.WriteError(string.Format("Invalid SharePoint site link or document library name: {0}, {1}", spSiteLink, spDocLibName));
            }
            else
            {
                if(bSimulateMigration == true)
                {
                    LogManager.Instance.WriteDebug(string.Format(" Uploading (simulated): {0} ", destDocLibPath + "/" + destinationFile));
                    return true;
                }

                ClientContext context = new ClientContext(spSiteLink);
                context.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                LogManager.Instance.WriteDebug(string.Format(" CreateSPFolderPath: {0} ", destDocLibPath));

                try
                {
                    var spFolder = CreateSPFolderPath(context, destDocLibPath);

                    if (spFolder == null)
                    {
                        LogManager.Instance.WriteError(string.Format("spFolder is null: {0}, {1}, {2}, {3}", sourceFolder, sourceFile, destDocLibPath, destinationFile));
                    }
                    else
                    {
                        LogManager.Instance.WriteDebug(string.Format(" Uploading: {0} ", spFolder.ServerRelativeUrl + "/" + destinationFile));

                        DirectoryInfo dInfo = new DirectoryInfo(sourceFolder);
                        SP2K16FileHelper.UploadDocument(context, sourceFolder + "/" + sourceFile, spFolder.ServerRelativeUrl + "/" + destinationFile);
                    }
                }
                catch (Exception exp)
                {
                    LogManager.Instance.WriteError(exp.Message);
                    return false;
                }            
            }

            return true;
        }

        private Folder CreateSPFolderPath(ClientContext context, string destinationFolderPath)
        {
            //This method checks if a particular SP folder exists in a document library. If not, it creates the heirarchy. 

            Web web = context.Web;
            var query = context.LoadQuery(web.Lists.Where(p => p.Title == spDocLibName));
            context.ExecuteQuery();
            
            List documentsLibrary = query.FirstOrDefault();
            Folder spFolderPath = null;

            if (documentsLibrary != null)
            {                
                var rootFolder = documentsLibrary.RootFolder;

                if(string.IsNullOrEmpty(destinationFolderPath) == true)
                {
                    context.Load(rootFolder);
                    context.ExecuteQuery();
                    return rootFolder;
                }

                var docLibFolders = rootFolder.Folders;
                context.Load(docLibFolders);
                context.ExecuteQuery();

                spFolderPath = rootFolder;

                var folders = destinationFolderPath.Split(new char[] { '\\', '/' }).ToList();     

                for (int i = 0;  i < folders.Count; i++)
                {                    
                    var folder = folders[i];                    
                    Folder spFolder = null;

                    context.Load(spFolderPath.Folders);
                    context.ExecuteQuery();
                    spFolder = spFolderPath.Folders.Where(f => f.Name == folder).FirstOrDefault();

                    if (spFolder == null || spFolder.Exists == false) // create
                    {                        
                        spFolderPath = spFolderPath.Folders.Add(folder);
                        context.Load(spFolderPath);
                        context.ExecuteQuery();

                        LogManager.Instance.WriteDebug(string.Format("   Created folder: {0} ", folder));
                    }
                    else // if the folder is already there
                    {
                        spFolderPath = spFolder;
                        LogManager.Instance.WriteDebug(string.Format("   Folder exists: {0} ", spFolderPath.Name));
                    }
                }
            }
            else
                LogManager.Instance.WriteError(string.Format("documentsLibrary is null: {0}, {1}", spDocLibName, destinationFolderPath));

            return spFolderPath;
        }
    }
}
