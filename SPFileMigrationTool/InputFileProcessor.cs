﻿using SPFileMigrationTool.Log;
using System;
using System.Collections.Generic;
using System.IO;

namespace SPFileMigrationTool.FileProcessor
{
    public class InputFileProcessor
    {
        readonly string inputFile = "";

        public InputFileProcessor(string file)
        {
            if(string.IsNullOrEmpty(file) || File.Exists(file) == false)
            {
                throw new FileNotFoundException(string.Format("Input file not found of invalid path: {0}", file));
            }

            inputFile = file;
        }

        // Path, Filename.ext, New SitePath, New DocLib Name, New Path (DocLib\folder\path), New File Name
        public Dictionary<string, MigrationData> GetMigrationData()
        {
            LogManager.Instance.WriteInformation(string.Format("Parsing input file: {0}", inputFile));
            var migrationData = new Dictionary<string, MigrationData>();
            var nDuplicateEntries = 0;

            try
            {
                using (StreamReader reader = new StreamReader(inputFile))
                {
                    var lineStr = reader.ReadLine();

                    while (lineStr != null)
                    {
                        if (lineStr.StartsWith("#") == true)
                        {
                            lineStr = reader.ReadLine();
                            continue; // comment
                        }

                        var tokens = lineStr.Split(new char[] { ';', ',', '\t' });

                        if (tokens.Length != 6) // Path, Filename.ext, New SitePath, New DocLib Name, New Path (DocLib\folder\path), New File Name
                        {
                            LogManager.Instance.WriteError(string.Format("Invalid data on line: {0}", lineStr));
                        }
                        else if(string.IsNullOrEmpty(tokens[0]) == true ||
                                string.IsNullOrEmpty(tokens[1]) == true ||
                                string.IsNullOrEmpty(tokens[2]) == true ||
                                string.IsNullOrEmpty(tokens[3]) == true)
                        {
                            LogManager.Instance.WriteError(string.Format("Invalid/Empty data on line: {0}", lineStr));
                        }
                        else
                        {
                            string key = tokens[0] + "\\" + tokens[1];

                            if (migrationData.ContainsKey(key) == true)
                            {
                                nDuplicateEntries++;
                                LogManager.Instance.WriteError(string.Format("Duplicate entry found, skipping it : {0}, {1}", tokens[0], tokens[1]));
                                lineStr = reader.ReadLine();
                                continue;
                            }

                            migrationData.Add(key, new MigrationData()
                            {
                                SourcePath          = tokens[0],    // Path     
                                SourceFileName      = tokens[1],    // Filename.ext
                                DestSiteLink        = tokens[2],    // New SitePath
                                DestDocumentLibName = tokens[3],    // New DocLib Name
                                DestDocumentLibPath = tokens[4],    // New Path (DocLib\folder\path)
                                DestFileName        = tokens[5],    // New File Name
                            });

                        }

                        lineStr = reader.ReadLine();
                    }

                    reader.Close();
                }

                LogManager.Instance.WriteInformation(string.Format("{0} valid iteams found in {1}. Duplicates: {2}", migrationData.Count.ToString(), inputFile, nDuplicateEntries));
            }
            catch(Exception exp)
            {
                LogManager.Instance.WriteError(exp.Message);
            }
            
            return migrationData;
        }


    }

    public struct MigrationData
    {
        public string SourcePath { get; set; }
        public string SourceFileName { get; set; }

        public string SourceFileNameWithPath
        {
            get
            {
                return string.Format(@"{0}\{1}", SourcePath, SourceFileName);
            }
        }

        public string DestSiteLink { get; set; }        //"http://dev.myspsite.com/sites/devcorpnet/MyWorkspace"
        public string DestDocumentLibName { get; set; } 
        public string DestDocumentLibPath { get; set; }
        public string DestFileName { get; set; }        
    }


}
