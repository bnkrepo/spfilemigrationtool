﻿using Microsoft.SharePoint.Client;
using SPFileMigrationTool.DB;
using SPFileMigrationTool.FileProcessor;
using SPFileMigrationTool.Log;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SPFileMigrationTool
{
    public partial class MainForm : System.Windows.Forms.Form
    {
        Dictionary<string, MigrationData> migrationData = null;

        bool SimulateMigration
        {
            get { return chkSimulateMigration.Checked; }
        }

        public string  InputFilePath { get; set; }

        public MainForm()
        {
            InitializeComponent();

            LogManager.Instance.OnWriteLog += OnWriteLog;
            SetLogLevel();

            InputFilePath = Properties.Settings.Default.InputFilePath;
        }

        private void SetLogLevel()
        {
            LogManager.Instance.ShowDebug   = chkShowDebug.Checked;
            LogManager.Instance.ShowErrors  = true;
            LogManager.Instance.ShowInfo    = true;
            LogManager.Instance.ShowWarning = true;
        }

        private void OnWriteLog(string strLog)
        {
            Invoke(new Action(() =>
            {
                if (chkShowLog.Checked == true)
                {
                    if(chkAutoAcroll.Checked == true)
                        txtLog.AppendText(strLog);
                    else
                    {
                        txtLog.Text += strLog;
                    }
                }
            }));
           
        }

        private async void btnStart_Click(object sender, EventArgs e)
        {
            // Clean-up
            txtLog.Clear();

            if (Directory.Exists(Properties.Settings.Default.FileDownloadFolder) == true)
            {
                // Delete all temp files.
                DirectoryInfo di = new DirectoryInfo(Properties.Settings.Default.FileDownloadFolder);
                foreach (FileInfo file in di.EnumerateFiles())
                {
                    file.Delete();
                }
            }

            // Start
            LogManager.Instance.WriteInformation(string.Format("Starting Migration at {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss tt")));

            // Process input file.
            var inputDataProcessor = new InputFileProcessor(InputFilePath);
            migrationData = inputDataProcessor.GetMigrationData();

            if (migrationData == null || migrationData.Count == 0)
            {
                LogManager.Instance.WriteInformation("Cannot continue the operation.");
                return;
            }

            SetProgressMax(migrationData.Count);
            int errorCount = 0;

            Task task = Task.Factory.StartNew( () =>
            {
                EnableControls(false);
                // Get files from SP2003 SQL DB
                SPDBHandler dbHandler = new SPDBHandler(Properties.Settings.Default.ConnectionString);
                var fileNameMap = dbHandler.DownloadSPFiles(migrationData,
                                                            Properties.Settings.Default.FileDownloadFolder,
                                                            SimulateMigration,
                                                            SetProgress,
                                                            SetStatusText);

                // Upload it to sp2016 
                if (fileNameMap != null && fileNameMap.Count > 0 && 
                    Directory.Exists(Properties.Settings.Default.FileDownloadFolder) == false)
                {
                    LogManager.Instance.WriteError(string.Format("Invalid source folder path: {0}", Properties.Settings.Default.FileDownloadFolder));
                    FinishProcessing();
                    return;
                }


                SP2K16Uploader sp2K16Uploader = new SP2K16Uploader();
                var strStatus = "Uploading files to SP2016...";
                LogManager.Instance.WriteInformation(strStatus);
                var sourceFiles = SimulateMigration == true ? dbHandler.SimulatedDownloadFileList : Directory.GetFiles(Properties.Settings.Default.FileDownloadFolder).Select(Path.GetFileName).ToList();

                if (sourceFiles == null || sourceFiles.Count == 0)
                {
                    LogManager.Instance.WriteInformation("No files found to be uploaded.");
                    FinishProcessing(); 
                    return;
                }

                SetProgressMax(sourceFiles.Count);
                SetProgress(0, strStatus);

                LogManager.Instance.WriteDebug(string.Format("Files in the folder: {0}", sourceFiles.Count));

                sourceFiles.ForEach(sourceFile =>
                {

                    var ret = sp2K16Uploader.Upload(Properties.Settings.Default.FileDownloadFolder,
                                                      sourceFile,
                                                      migrationData[fileNameMap[sourceFile]].DestSiteLink,
                                                      migrationData[fileNameMap[sourceFile]].DestDocumentLibName,
                                                      migrationData[fileNameMap[sourceFile]].DestDocumentLibPath,
                                                      string.IsNullOrEmpty(migrationData[fileNameMap[sourceFile]].DestFileName) == true ?
                                                                                migrationData[fileNameMap[sourceFile]].SourceFileName :
                                                                                migrationData[fileNameMap[sourceFile]].DestFileName,
                                                       SimulateMigration);
                    if (ret == true)
                        SetProgressNext(string.Format("Uploaded {0}", sourceFile));
                    else
                    {
                        LogManager.Instance.WriteError(string.Format("Upload failed: {0}", sourceFile));
                        errorCount++;
                    }
                });

                LogManager.Instance.WriteInformation("Uploading files to SP2016 completed.");   
                
                if(errorCount > 0)
                    LogManager.Instance.WriteInformation($"{errorCount} error(s) occured during this operarion. Check the log for more details.");

                FinishProcessing();
            });

            List<Task> tasks = new List<Task>
            {
                task
            };
            
            await Task.WhenAll(tasks.ToArray());

            LogManager.Instance.WriteInformation(string.Format("Migration completed at {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss tt")));
        }

        private void FinishProcessing()
        {
            SetStatusText("Completed.");
            EnableControls();
        }

        private void SetProgressMax(int value)
        {
            Invoke(new Action(() =>
            {
                LogManager.Instance.WriteInformation(string.Format("SetProgressMax: {0}", value));
                progressBar.Maximum = value;
            }));            
        }

        private void EnableControls(bool bEnable = true)
        {
            Invoke(new Action(() =>
            {
                btnStart.Enabled = bEnable;
                btnSaveLog.Enabled = bEnable;
                btnBrowseInputFile.Enabled = bEnable;
                chkShowDebug.Enabled = bEnable;
                chkSimulateMigration.Enabled = bEnable;
            }));
        }

        private void SetProgress(int value, string strStatus)
        {
            Invoke(new Action(() =>
            {
                if(progressBar.Value >= value)
                    progressBar.Value = value;
                lblStatus.Text = strStatus;
            }));
        }

        private void SetProgressNext(string strStatus)
        {
            Invoke(new Action(() =>
            {
                progressBar.Value++;
                lblStatus.Text = strStatus;
            }));
        }

        private void SetStatusText(string strStatus)
        {
            Invoke(new Action(() =>
            {
                lblStatus.Text = strStatus;
            }));
        }


        private void btnSaveLog_Click(object sender, EventArgs e)
        {
            if(saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.File.WriteAllText(saveFileDialog.FileName, txtLog.Text);
            }
        }       

        private void chkShowDebug_CheckedChanged(object sender, EventArgs e)
        {
            SetLogLevel();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ShowInputFileUsed();
        }

        private void ShowInputFileUsed()
        {
            LogManager.Instance.WriteInformation(string.Format("Input file being used: {0}", InputFilePath));
        }

        private void btnBrowseInputFile_Click(object sender, EventArgs e)
        {
            if(openFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtInputFilePath.Text = openFileDialog.FileName;
                InputFilePath = openFileDialog.FileName;
                ShowInputFileUsed();

            }
        }
    }
}
