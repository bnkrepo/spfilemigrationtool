﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPFileMigrationTool
{
    public static class StringExtensions
    {
        public static bool IncludesFilePath(this string str)
        {
            return str.Contains("\\") || str.Contains("/");
        }
    }
}
