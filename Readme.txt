This application is developed to migrate selected files from SharePoint 2003 to SharePoint 2016.  
The tool is developed in C# WinForms project in Visual Studio 2017. It needs .NET framework 4.6.1 or above to run.

The files are extracted from SQL database and uploaded to SharePoint 2016.

More details: https://bnkalse.wordpress.com/2018/11/13/sharepoint-2003-to-2016-file-migration-tool/